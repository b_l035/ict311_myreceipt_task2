package com.bignerdranch.android.myreceipts.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.bignerdranch.android.myreceipts.Receipt;
import com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable;

import java.util.Date;
import java.util.UUID;

import static com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable.*;

public class ReceiptCursorWrapper extends CursorWrapper {

    public ReceiptCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Receipt getReceipt() {
        String uuidString = getString(getColumnIndex(Cols.UUID));
        String title = getString(getColumnIndex(Cols.TITLE));
        String shop = getString(getColumnIndex(Cols.SHOP));
        long date = getLong(getColumnIndex(Cols.DATE));
        String comment = getString(getColumnIndex(Cols.COMMENT));
        int isPaid = getInt(getColumnIndex(Cols.PAID));
        String suspect = getString(getColumnIndex(ReceiptTable.Cols.SUSPECT));

        Receipt receipt = new Receipt(UUID.fromString(uuidString));
        receipt.setTitle(title);
        receipt.setShop(shop);
        receipt.setDate(new Date(date));
        receipt.setComments(comment);
        receipt.setPaid(isPaid != 0);
        receipt.setSuspect(suspect);

        return receipt;
    }
}
