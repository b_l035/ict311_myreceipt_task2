package com.bignerdranch.android.myreceipts;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bignerdranch.android.myreceipts.database.ReceiptBaseHelper;
import com.bignerdranch.android.myreceipts.database.ReceiptCursorWrapper;
import com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable.Cols.COMMENT;
import static com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable.Cols.DATE;
import static com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable.Cols.SHOP;
import static com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable.Cols.PAID;
import static com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable.Cols.TITLE;
import static com.bignerdranch.android.myreceipts.database.ReceiptDbSchema.ReceiptTable.Cols.UUID;

public class ReceiptLab {
    private static ReceiptLab sReceiptLab;
    private Context mContext;
    private static SQLiteDatabase mDatabase;
    private List<Receipt> mReceipt;

    public static ReceiptLab get(Context context) {
        if (sReceiptLab == null) {
            sReceiptLab = new ReceiptLab(context);
        }

        return sReceiptLab;
    }

    private ReceiptLab(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new ReceiptBaseHelper(mContext)
                .getWritableDatabase();
        mReceipt = new ArrayList<>();

    }

    public void addReceipt(Receipt c) {
        ContentValues values = getContentValues(c);
        mDatabase.insert(ReceiptTable.NAME, null, values);
    }

    public List<Receipt> getReceipts() {
        List<Receipt> receipts = new ArrayList<>();
        ReceiptCursorWrapper cursor = queryReceipts(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                receipts.add(cursor.getReceipt());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return receipts;
    }

    public Receipt getReceipt(UUID id) {
        ReceiptCursorWrapper cursor = queryReceipts(
                ReceiptTable.Cols.UUID + " = ?",
                new String[]{id.toString()}
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getReceipt();
        } finally {
            cursor.close();
        }
    }

    public void updateReceipt(Receipt receipt) {
        String uuidString = receipt.getId().toString();
        ContentValues values = getContentValues(receipt);
        mDatabase.update(ReceiptTable.NAME, values,
                ReceiptTable.Cols.UUID + " = ?",
                new String[] { uuidString });
    }


    public void deleteReceipt(UUID receiptId)
    {
        String uuidString = receiptId.toString();

        mDatabase.delete(ReceiptTable.NAME, ReceiptTable.Cols.UUID + " = ?", new String[] {uuidString});
    }


    public File getPhotoFile(Receipt receipt) {
        File filesDir = mContext.getFilesDir();
        return new File(filesDir, receipt.getPhotoFilename());
    }

    private ReceiptCursorWrapper queryReceipts(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                ReceiptTable.NAME,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null  // orderBy
        );
        return new ReceiptCursorWrapper(cursor);
    }

    private static ContentValues getContentValues(Receipt receipt) {
        ContentValues values = new ContentValues();
        values.put(UUID, receipt.getId().toString());
        values.put(TITLE, receipt.getTitle());
        values.put(SHOP, receipt.getShop());
        values.put(DATE, receipt.getDate().getTime());
        values.put(COMMENT, receipt.getComment());
        values.put(PAID, receipt.isPaid() ? 1 : 0);
        values.put(ReceiptTable.Cols.SUSPECT, receipt.getSuspect());

        return values;
    }
}
